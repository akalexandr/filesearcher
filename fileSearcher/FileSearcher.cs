﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Threading;

namespace fileSearcher
{
    [Serializable]
    class FileSearcher
    {
        private List<string> inputFileList = new List<string>();
        private List<string> subDirectories;
        private ManualResetEvent manualResetEvent;
        double fileCount = 0;
        Stopwatch stopWatch = new Stopwatch();

        List<string> skipExtensions = new List<string> { ".dll", ".sys", ".exe", ".chm", ".CAB"};
        public List<string> fileList
        {
            get
            {
                return inputFileList;
            }
        }


        public ManualResetEvent ResetEvent
        {
            get
            {
                return manualResetEvent;
            }
        }
        public async void AsyncSearchFile(TextBox lbCurFile, TextBox lbCurNum, TextBox lbCurTime, string directory = "", string searchingPattern = null, string searchingTextInFile = null, int num = 0)
        {
            while(true)
            {
                await Task.Run(() => SearchFile(lbCurFile, lbCurNum, lbCurTime, directory, searchingPattern, searchingTextInFile, num));
                manualResetEvent.Set();
            }
        }

        public void SearchFile(TextBox lbCurFile, TextBox lbCurNum, TextBox lbCurTime, string directory = "", string searchingPattern = null, string searchingTextInFile = null, int num = 0)
        {
            List<string> outputFileList = new List<string>();
            
            stopWatch?.Start();

            subDirectories = Directory.GetDirectories(directory).ToList();
            CheckAvailibility(ref subDirectories);
            List<string> filePaths = Directory.GetFiles(directory).ToList();

            foreach (string filePath in filePaths)
            {
                fileCount++;
                AddToInformation(lbCurNum, fileCount.ToString() + " шт.");
                AddToInformation(lbCurFile, Path.GetFileName(filePath));
                AddToInformation(lbCurTime, Math.Round(stopWatch.Elapsed.TotalSeconds,2).ToString() + " сек.");


                if (IsSystem(filePath))
                    continue;
                if (CheckExtension(filePath))
                {
                    string checkedFile = SearchInFile(filePath, searchingPattern, searchingTextInFile);
                    if (checkedFile != null)
                        inputFileList.Add(checkedFile);
                }
                else
                {
                    if (MaskCompare(filePath, searchingPattern))
                        inputFileList.Add(filePath);
                }

            }

            foreach (string subDirectory in subDirectories)
            {
                if (IsSystem(subDirectory))
                    continue;

                SearchFile(lbCurFile, lbCurNum, lbCurTime, subDirectory, searchingPattern, searchingTextInFile);
            }
            
        }


        private void AddToInformation(TextBox textBox, string text)
        {
            Action action = () => textBox.Text = text;
            if (textBox.InvokeRequired)
                try
                {
                    textBox.Invoke(action);
                }
                catch
                {
                    return;
                }
        }

        private bool CheckExtension(string path)
        {
            string extension = Path.GetExtension(path);
            foreach(string ext in skipExtensions)
            {
                if (ext == extension)
                {
                    return false;
                }
            }
            return true;
        }
        private bool MaskCompare(string str, string mask)
        {
            if (mask == null)
                return true;
            Regex regex = new Regex(mask, RegexOptions.Compiled | RegexOptions.IgnoreCase);

            return ((regex.Match(str).Index != 0) && str.Contains(mask));
        }

        private string SearchInFile(string filePath, string searchingPattern = "*.*", string searchingTextInFile = null)
        {
            if (MaskCompare(filePath, searchingPattern))
            {
                if (searchingTextInFile != null)
                {
                    using (StreamReader reader = new StreamReader(filePath))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            if (line.Contains(searchingTextInFile))
                            {
                                return filePath;
                            }
                        }
                    }
                }
                else
                {
                    return filePath;
                }
            }
            return null;
        }

        private void CheckAvailibility(ref List<string> inputList)
        {
            try
            {
                foreach (string dir in inputList)
                {
                    try
                    {
                        List<string> fileList = Directory.GetFiles(dir).ToList();
                        FileAttributes fileAttributes = File.GetAttributes(dir);
                        if (((fileAttributes & FileAttributes.System) | (fileAttributes & FileAttributes.Hidden)) != 0)
                        {
                            inputList.Remove(dir);
                            CheckAvailibility(ref inputList);
                        }
                    }
                    catch
                    {
                        inputList.Remove(dir);
                        CheckAvailibility(ref inputList);
                    }
                }

            }
            catch
            {
                return;
            }
        }


        private bool IsSystem(string path)
        {
            FileAttributes fileAttributes = File.GetAttributes(path);
            return ((fileAttributes & FileAttributes.System) | (fileAttributes & FileAttributes.Hidden)) != 0;
        }
    }
}
