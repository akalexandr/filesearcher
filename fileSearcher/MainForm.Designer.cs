﻿namespace fileSearcher
{
    public partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.fileList = new System.Windows.Forms.TreeView();
            this.startDirectory = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.fileText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.fileName = new System.Windows.Forms.TextBox();
            this.start = new System.Windows.Forms.Button();
            this.pauseButton = new System.Windows.Forms.Button();
            this.getFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.stopButton = new System.Windows.Forms.Button();
            this.restart = new System.Windows.Forms.Button();
            this.curTime = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.curNum = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.curFile = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fileList
            // 
            this.fileList.Location = new System.Drawing.Point(12, 12);
            this.fileList.Name = "fileList";
            this.fileList.ShowNodeToolTips = true;
            this.fileList.Size = new System.Drawing.Size(583, 365);
            this.fileList.TabIndex = 0;
            // 
            // startDirectory
            // 
            this.startDirectory.Location = new System.Drawing.Point(109, 23);
            this.startDirectory.Name = "startDirectory";
            this.startDirectory.Size = new System.Drawing.Size(471, 22);
            this.startDirectory.TabIndex = 3;
            this.startDirectory.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.dblClick_StartDirectory);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Папка:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.fileText);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.fileName);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.startDirectory);
            this.groupBox1.Location = new System.Drawing.Point(12, 441);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(586, 112);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Параметры поиска";
            // 
            // fileText
            // 
            this.fileText.Location = new System.Drawing.Point(109, 79);
            this.fileText.Name = "fileText";
            this.fileText.Size = new System.Drawing.Size(471, 22);
            this.fileText.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Текст в файле";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Имя файла";
            // 
            // fileName
            // 
            this.fileName.Location = new System.Drawing.Point(109, 51);
            this.fileName.Name = "fileName";
            this.fileName.Size = new System.Drawing.Size(471, 22);
            this.fileName.TabIndex = 5;
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(12, 559);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(142, 29);
            this.start.TabIndex = 17;
            this.start.Text = "начать";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // pauseButton
            // 
            this.pauseButton.Location = new System.Drawing.Point(308, 559);
            this.pauseButton.Name = "pauseButton";
            this.pauseButton.Size = new System.Drawing.Size(142, 29);
            this.pauseButton.TabIndex = 18;
            this.pauseButton.Text = "приостановить";
            this.pauseButton.UseVisualStyleBackColor = true;
            this.pauseButton.Click += new System.EventHandler(this.pauseButton_Click);
            // 
            // stopButton
            // 
            this.stopButton.Location = new System.Drawing.Point(456, 559);
            this.stopButton.Name = "stopButton";
            this.stopButton.Size = new System.Drawing.Size(142, 29);
            this.stopButton.TabIndex = 18;
            this.stopButton.Text = "прервать";
            this.stopButton.UseVisualStyleBackColor = true;
            this.stopButton.Click += new System.EventHandler(this.stopButton_Click);
            // 
            // restart
            // 
            this.restart.Location = new System.Drawing.Point(160, 559);
            this.restart.Name = "restart";
            this.restart.Size = new System.Drawing.Size(142, 29);
            this.restart.TabIndex = 17;
            this.restart.Text = "заново";
            this.restart.UseVisualStyleBackColor = true;
            this.restart.Click += new System.EventHandler(this.restart_Click);
            // 
            // curTime
            // 
            this.curTime.Location = new System.Drawing.Point(427, 413);
            this.curTime.Name = "curTime";
            this.curTime.Size = new System.Drawing.Size(168, 22);
            this.curTime.TabIndex = 30;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(320, 416);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 17);
            this.label4.TabIndex = 29;
            this.label4.Text = "Прошло (сек.)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 416);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 17);
            this.label5.TabIndex = 27;
            this.label5.Text = "Количество";
            // 
            // curNum
            // 
            this.curNum.Location = new System.Drawing.Point(103, 413);
            this.curNum.Name = "curNum";
            this.curNum.Size = new System.Drawing.Size(141, 22);
            this.curNum.TabIndex = 28;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 388);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 17);
            this.label6.TabIndex = 25;
            this.label6.Text = "Текущий файл";
            // 
            // curFile
            // 
            this.curFile.Location = new System.Drawing.Point(124, 383);
            this.curFile.Name = "curFile";
            this.curFile.Size = new System.Drawing.Size(471, 22);
            this.curFile.TabIndex = 26;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 601);
            this.Controls.Add(this.curTime);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.curNum);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.curFile);
            this.Controls.Add(this.stopButton);
            this.Controls.Add(this.pauseButton);
            this.Controls.Add(this.restart);
            this.Controls.Add(this.start);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.fileList);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Список файлов";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Closing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView fileList;
        private System.Windows.Forms.TextBox startDirectory;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Button pauseButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox fileName;
        private System.Windows.Forms.TextBox fileText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.FolderBrowserDialog getFolder;
        private System.Windows.Forms.Button stopButton;
        private System.Windows.Forms.Button restart;
        private System.Windows.Forms.TextBox curTime;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox curNum;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox curFile;
    }
}

