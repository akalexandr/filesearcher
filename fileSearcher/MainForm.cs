﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace fileSearcher
{
    public partial class MainForm : Form
    {
        FileSearcher fileSearcher = new FileSearcher();
        Thread thread, check;
        ManualResetEvent manualResetEvent = new ManualResetEvent(true);


        public MainForm()
        {
            InitializeComponent();
            start.Text = "начать";
        }
        private void restart_Click(object sender, EventArgs e)
        {
            if (((thread.ThreadState & ThreadState.Aborted) != ThreadState.Aborted) && 
                ((thread.ThreadState & ThreadState.Suspended) != ThreadState.Suspended))
            {
                check?.Abort();
                thread?.Abort();
                fileSearcher = new FileSearcher();
                fileList.Nodes.Clear();

                AsyncSearchFile();
                CheckThreadState();
            }
            else
            {
                manualResetEvent.Close();
                manualResetEvent = new ManualResetEvent(true);
                fileSearcher = new FileSearcher();
                fileList.Nodes.Clear();

                AsyncSearchFile();
                CheckThreadState();
            }
        }

        private void start_Click(object sender, EventArgs e)
        {
            if (startDirectory.Text == "")
            {
                if (MessageBox.Show("Укажите стартовую папку!\nДля этого нажмите двойным кликом по соответствующему полю (Папка).", "Не указана стартовая папка", MessageBoxButtons.OK) == DialogResult.OK)
                {
                    return;
                }
            }

            
            switch(start.Text)
            {
                case "начать":
                    start.Text = "продолжить";
                    AsyncSearchFile();
                    CheckThreadState();
                    break;
                case "продолжить":
                        if ((thread.ThreadState & ThreadState.Suspended) == ThreadState.Suspended)
                            thread.Resume();
                    break;
                default:
                    break;
            }
        }

        private TreeNode AddNode(TreeView fileList, string nodeValue, TreeNode inputNode = null)
        {
            TreeNode node = null;
            Action action = null;
            if (inputNode == null)
            {
                action = () => node = fileList.Nodes.Add(nodeValue);
            }
            else
            {
                action = () => node = inputNode.Nodes.Add(nodeValue);
            }

            if (curFile.InvokeRequired)
                curFile.Invoke(action);

            return node;
        }

        private void ReNameButton(Button button, string text)
        {
            Action action = () => button.Text = text;
            if (button.InvokeRequired)
            {
                button.Invoke(action);
            }
        }

        private void CheckThreadState()
        {
            check = new Thread(() =>
            {
                while(true)
                {
                    if (thread.ThreadState == ThreadState.Aborted || !thread.IsAlive)
                    {
                        TreeNode node = AddNode(fileList, startDirectory.Text);
                        List<string> dirList = new List<string>();

                        foreach (string file in fileSearcher.fileList)
                        {
                            dirList.Add(Path.GetDirectoryName(file));
                        }

                        dirList = dirList.Distinct().ToList();

                        foreach (string dir in dirList)
                        {
                            TreeNode childNode = AddNode(fileList, dir, node);
                            foreach (string file in fileSearcher.fileList)
                            {
                                if (Path.GetDirectoryName(file) == dir)
                                {
                                    TreeNode val = AddNode(fileList, file, childNode);
                                }
                            }
                        }

                        ReNameButton(start, "начать");
                        check.Abort();
                        break;
                    }
                    
                }
            });
            check.IsBackground = true;
            check.Start();
        }


        private void AsyncSearchFile()
        {
            thread = new Thread(() =>
            {
                fileSearcher.SearchFile(curFile, curNum, curTime, startDirectory.Text, fileName.Text == "" ? null : fileName.Text, 
                                                                      fileText.Text == "" ? null : fileText.Text);
            });
            manualResetEvent.Set();
            thread.IsBackground = true;
            thread.Start();
        }

        private void pauseButton_Click(object sender, EventArgs e)
        {
            manualResetEvent.Reset();
            thread.Suspend();
        }

        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            string saver = Path.GetDirectoryName(Application.ExecutablePath) + "\\Saver.txt";
            using (StreamWriter sw = File.CreateText(saver))
            {
                sw.WriteLine(startDirectory.Text);
                sw.WriteLine(fileName.Text);
                sw.WriteLine(fileText.Text);
            }

            manualResetEvent.Close();
            if ((check.ThreadState & ThreadState.Aborted) != ThreadState.Aborted)
                check?.Abort();

            if ((thread.ThreadState & ThreadState.Aborted) != ThreadState.Aborted)
                thread?.Abort();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            string saver = Path.GetDirectoryName(Application.ExecutablePath) + "\\Saver.txt";

            if (File.Exists(saver))
            {
                using (StreamReader sr = File.OpenText(saver))
                {
                    string line = "";
                    int lineNum = 0;
                    while ((line = sr.ReadLine()) != null)
                    {
                        lineNum++;
                        switch(lineNum)
                        {
                            case 1:
                                startDirectory.Text = line;
                                break;
                            case 2:
                                fileName.Text = line;
                                break;
                            case 3:
                                fileText.Text = line;
                                break;
                        }
                    }
                }

                File.Delete(saver);
            }
        }

        private void stopButton_Click(object sender, EventArgs e)
        {
            if (((thread.ThreadState & ThreadState.Aborted) != ThreadState.Aborted) &&
                ((thread.ThreadState & ThreadState.Suspended) != ThreadState.Suspended))
            {
                thread?.Abort();
            }
            else
            {
                MessageBox.Show("Нельзя прервать остановленный поток!");
            }

        }

        private void dblClick_StartDirectory(object sender, MouseEventArgs e)
        {
            FolderBrowserDialog FBD = new FolderBrowserDialog();
            if (FBD.ShowDialog() == DialogResult.OK)
            {
                startDirectory.Text = FBD.SelectedPath;
            }
        }
    }
}
